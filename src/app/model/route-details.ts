import { RouteSchedule } from './route-schedule';
import { Address } from 'cluster';

export class RouteDetails {

  originAddress: Address;
  destinationAddress: Address;
  routeSchedule: RouteSchedule;
}
