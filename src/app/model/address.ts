import { GeoPoint } from './geo-point';

export class Address {

  description: string;
  point: GeoPoint;
}
