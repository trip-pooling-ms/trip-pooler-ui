import { WeekDay } from '@angular/common';

export class RouteSchedule {

  departureTime: Date;
  returnTime: Date;
  dayOfWeeks: WeekDay[];
}
