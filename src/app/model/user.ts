import { RouteDetails } from './route-details';

export class User {

  id: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  routeDetails: RouteDetails;
}
